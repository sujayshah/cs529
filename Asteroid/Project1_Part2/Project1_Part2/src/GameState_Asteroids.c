﻿/* Start Header -------------------------------------------------------
Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Game_Asteroids.c
Purpose:  2D Asteroid game implementation
Language: C/C++ using Visual Studio 2015
Platform: Visual Studio 2015,
Hardware Requirements: 	1.6 GHz or faster processor
1 GB of RAM (1.5 GB if running on a virtual machine)
4 GB of available hard disk space 5400 RPM hard disk drive
DirectX 9-capable video card (1024 x 768 or higher resolution)
Operating Systems:	Windows 10
Windows 8.1
Windows 8
Windows 7 SP 1
Windows Server 2012 R2
Windows Server 2012
Windows Server 2008 R2 SP1

Project: 		CS529_sujay.shah_2

Author: Name: Sujay Shah ;  Login: sujay.shah  ; Student ID : 60001517
Creation date: September 27, 2017.
- End Header --------------------------------------------------------*/

#include "main.h"
#include "Math2D.h"
#include "Matrix2D.h"
#include "Vector2D.h"

// ---------------------------------------------------------------------------
// Defines

#define SHAPE_NUM_MAX				32					// The total number of different vertex buffer (Shape)
#define GAME_OBJ_INST_NUM_MAX		2048				// The total number of different game object instances


// Feel free to change these values in ordet to make the game more fun
#define SHIP_INITIAL_NUM			3					// Initial number of ship lives
#define SHIP_SIZE					100.0f				// Ship size
#define SHIP_ACCEL_FORWARD			250.0f				// Ship forward acceleration (in m/s^2)
#define SHIP_ACCEL_BACKWARD			-200.0f				// Ship backward acceleration (in m/s^2)
#define SHIP_ROT_SPEED				(2.0f * PI)			// Ship rotation speed (radian/second)
#define HOMING_MISSILE_ROT_SPEED	(PI / 2.0f)			// Homing missile rotation speed (radian/second)
#define BULLET_SPEED				500.0f				// Bullet speed (m/s)

// ---------------------------------------------------------------------------

enum OBJECT_TYPE
{
	// list of game object types
	OBJECT_TYPE_SHIP = 0,
	OBJECT_TYPE_BULLET,
	OBJECT_TYPE_ASTEROID,
	OBJECT_TYPE_HOMING_MISSILE,

	OBJECT_TYPE_NUM
};

// ---------------------------------------------------------------------------
// object mFlag definition

#define FLAG_ACTIVE		0x00000001

// ---------------------------------------------------------------------------
// Struct/Class definitions

typedef struct GameObjectInstance GameObjectInstance;			// Forward declaration needed, since components need to point to their owner "GameObjectInstance"

// ---------------------------------------------------------------------------

typedef struct 
{
	unsigned long			mType;				// Object type (Ship, bullet, etc..)
	AEGfxVertexList*		mpMesh;				// This will hold the triangles which will form the shape of the object

}Shape;

// ---------------------------------------------------------------------------

typedef struct
{
	Shape *mpShape;

	GameObjectInstance *	mpOwner;			// This component's owner
}Component_Sprite;

// ---------------------------------------------------------------------------

typedef struct
{
	Vector2D					mPosition;			// Current position
	float					mAngle;				// Current angle
	float					mScaleX;			// Current X scaling value
	float					mScaleY;			// Current Y scaling value

	Matrix2D					mTransform;			// Object transformation matrix: Each frame, calculate the object instance's transformation matrix and save it here

	GameObjectInstance *	mpOwner;			// This component's owner
}Component_Transform;

// ---------------------------------------------------------------------------

typedef struct
{
	Vector2D					mVelocity;			// Current velocity

	GameObjectInstance *	mpOwner;			// This component's owner
}Component_Physics;

// ---------------------------------------------------------------------------

typedef struct
{
	GameObjectInstance *		mpTarget;		// Target, used by the homing missile

	GameObjectInstance *		mpOwner;		// This component's owner
}Component_Target;

// ---------------------------------------------------------------------------


//Game object instance structure
struct GameObjectInstance
{
	unsigned long				mFlag;						// Bit mFlag, used to indicate if the object instance is active or not

	Component_Sprite			*mpComponent_Sprite;		// Sprite component
	Component_Transform			*mpComponent_Transform;		// Transform component
	Component_Physics			*mpComponent_Physics;		// Physics component
	Component_Target			*mpComponent_Target;		// Target component, used by the homing missile
};
double frameTime;
// ---------------------------------------------------------------------------
// Static variables

// List of original vertex buffers
static Shape				sgShapes[SHAPE_NUM_MAX];									// Each element in this array represents a unique shape 
static unsigned long		sgShapeNum;													// The number of defined shapes

// list of object instances
static GameObjectInstance		sgGameObjectInstanceList[GAME_OBJ_INST_NUM_MAX];		// Each element in this array represents a unique game object instance
static unsigned long			sgGameObjectInstanceNum;								// The number of active game object instances

// pointer ot the ship object
static GameObjectInstance*		sgpShip;												// Pointer to the "Ship" game object instance

// number of ship available (lives 0 = game over)
static long						sgShipLives;											// The number of lives left

// the score = number of asteroid destroyed
static unsigned long			sgScore;												// Current score

// ---------------------------------------------------------------------------

// functions to create/destroy a game object instance
static GameObjectInstance*			GameObjectInstanceCreate(unsigned int ObjectType);			// From OBJECT_TYPE enum
static void							GameObjectInstanceDestroy(GameObjectInstance* pInst);

// ---------------------------------------------------------------------------

// Functions to add/remove components
static void AddComponent_Transform(GameObjectInstance *pInst, Vector2D *pPosition, float Angle, float ScaleX, float ScaleY);
static void AddComponent_Sprite(GameObjectInstance *pInst, unsigned int ShapeType);
static void AddComponent_Physics(GameObjectInstance *pInst, Vector2D *pVelocity);
static void AddComponent_Target(GameObjectInstance *pInst, GameObjectInstance *pTarget);

static void RemoveComponent_Transform(GameObjectInstance *pInst);
static void RemoveComponent_Sprite(GameObjectInstance *pInst);
static void RemoveComponent_Physics(GameObjectInstance *pInst);
static void RemoveComponent_Target(GameObjectInstance *pInst);

// ---------------------------------------------------------------------------

// "Load" function of this state
void GameStateAsteroidsLoad(void)
{
	Shape* pShape = NULL;

	// Zero the shapes array
	memset(sgShapes, 0, sizeof(Shape) * SHAPE_NUM_MAX);
	// No shapes at this point
	sgShapeNum = 0;

	// The ship object instance hasn't been created yet, so this "sgpShip" pointer is initialized to 0
	sgpShip = 0;



	/// Create the game objects(shapes) : Ships, Bullet, Asteroid and Missile
	// How to:
	// -- Remember to create normalized shapes, which means all the vertices' coordinates should be in the [-0.5;0.5] range. Use the object instances' scale values to resize the shape.
	// -- Call “AEGfxMeshStart” to inform the graphics manager that you are about the start sending triangles.
	// -- Call “AEGfxTriAdd” to add 1 triangle.
	// -- A triangle is formed by 3 counter clockwise vertices (points).
	// -- Create all the points between (-0.5, -0.5) and (0.5, 0.5), and use the object instance's scale to change the size.
	// -- Each point can have its own color.
	// -- The color format is : ARGB, where each 2 hexadecimal digits represent the value of the Alpha, Red, Green and Blue respectively. Note that alpha blending(Transparency) is not implemented.
	// -- Each point can have its own texture coordinate (set them to 0.0f in case you’re not applying a texture).
	// -- An object (Shape) can have multiple triangles.
	// -- Call “AEGfxMeshEnd” to inform the graphics manager that you are done creating a mesh, which will return a pointer to the mesh you just created.

	

	// =====================
	// Create the ship shape
	// =====================

	pShape = sgShapes + sgShapeNum++;
	pShape->mType = OBJECT_TYPE_SHIP;

	AEGfxMeshStart();
	AEGfxTriAdd(
		-0.5f,  0.5f, 0xFFFF0000, 0.0f, 0.0f, 
		-0.5f, -0.5f, 0xFFFF0000, 0.0f, 0.0f,
		 0.5f,  0.0f, 0xFFFFFFFF, 0.0f, 0.0f); 
	pShape->mpMesh = AEGfxMeshEnd();


	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	// TO DO 4:
	// -- Create the bullet shape
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	pShape = sgShapes + sgShapeNum++;
	pShape->mType = OBJECT_TYPE_BULLET;

	AEGfxMeshStart();
	AEGfxTriAdd(
		0.3f, 0.03f, 0xFFdff401, 0.0f, 0.0f,
		-0.3f, 0.03f, 0xFFdff401, 0.0f, 0.0f,
		-0.3f, -0.03f, 0xFFdff401, 0.0f, 0.0f);

	AEGfxTriAdd(
		-0.3f, -0.03f, 0xFFdff401, 0.0f, 0.0f,
		0.3f, -0.03f, 0xFFdff401, 0.0f, 0.0f,
		 0.3f, 0.03f, 0xFFdff401, 0.0f, 0.0f);
		
	pShape->mpMesh = AEGfxMeshEnd();

	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	// TO DO 7:
	// -- Create the asteroid shape
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	pShape = sgShapes + sgShapeNum++;
	pShape->mType = OBJECT_TYPE_ASTEROID;

	AEGfxMeshStart();
	AEGfxTriAdd(
		0.5f, 0.5f, 0xFF808080, 0.0f, 0.0f,
		-0.5f, 0.5f, 0xFF808080, 0.0f, 0.0f,
		-0.5f, -0.5f, 0xFF808080, 0.0f, 0.0f);

	AEGfxTriAdd(
		-0.5f, -0.5f, 0xFF808080, 0.0f, 0.0f,
		0.5f, -0.5f, 0xFF808080, 0.0f, 0.0f,
		0.5f, 0.5f, 0xFF808080, 0.0f, 0.0f);

	pShape->mpMesh = AEGfxMeshEnd();


	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	// TO DO 10:
	// -- Create the homing missile shape
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	pShape = sgShapes + sgShapeNum++;
	pShape->mType = OBJECT_TYPE_HOMING_MISSILE;

	/*
	AEGfxMeshStart();
	pShape->mpMesh = AEGfxMeshEnd();
	AEGfxTriAdd(
		0.3f, -0.5f, 0xFFdff401, 0.0f, 0.0f,
		-0.5f, 0.5f, 0xFFdff401, 0.0f, 0.0f,
		-0.5f, -0.5f, 0xFFdff401, 0.0f, 0.0f);

	AEGfxTriAdd(
		0.3f, -0.5f, 0xFFdff401, 0.0f, 0.0f,
		0.3f, 0.5f, 0xFFdff401, 0.0f, 0.0f,
		-0.5f, 0.5f, 0xFFdff401, 0.0f, 0.0f);

	AEGfxTriAdd(
		0.3f, -0.5f, 0xFFdff401, 0.0f, 0.0f,
		0.5f, 0.0f, 0xFFdff401, 0.0f, 0.0f,
		0.3f, 0.5f, 0xFFdff401, 0.0f, 0.0f);
		pShape->mpMes+h = AEGfxMeshEnd();
		*/

	AEGfxMeshStart();
	AEGfxTriAdd(
		-0.5f, 0.15f, 0xFF1cfd6e, 0.0f, 0.0f,
		-0.5f, -0.15f, 0xFF1cfd6e, 0.0f, 0.0f,
		0.5f, 0.0f, 0xFF1cfd6e, 0.0f, 0.0f);
	pShape->mpMesh = AEGfxMeshEnd();
	


}
// CREATE ASTEROIDS
// ---------------------------------------------------------------------------
void Asteroid_create()
{
	float posX[10] = { 69.0f,-452.0f,42.0f,-41.0f ,-101.0f,-202.f,-403.0f,-401.0f,399.0f,398.0f };
	float posY[10] = { -302.0f,-201.0f,-110.0f,-51.0f,2.0f,52.0f,101.0f,151.0f,199.0f,251.0f };
	
	float  vxr, vyr, scx, scy, right, left, top, bot;
	int xr, yr;
	right = AEGfxGetWinMaxX();
	left = AEGfxGetWinMinX();
	top = AEGfxGetWinMaxY();
	bot = AEGfxGetWinMinY();
	GameObjectInstance* pInst = GameObjectInstanceCreate(OBJECT_TYPE_ASTEROID);
	if (pInst->mpComponent_Sprite->mpShape->mType == OBJECT_TYPE_ASTEROID)
	{

		xr = rand() % 10;
		yr = rand() % 10;
		vxr = rand() % 189;
		vyr = rand() % 150;
		scx = rand() % 90;
		scy = rand() % 80;
		Vector2DSet(&pInst->mpComponent_Transform->mPosition, posX[xr], posY [yr]);
		pInst->mpComponent_Physics->mVelocity.x = vxr;
		pInst->mpComponent_Physics->mVelocity.y = vyr;
		pInst->mpComponent_Transform->mScaleX = scx;
		pInst->mpComponent_Transform->mScaleY = scy;
	}
}
// "Initialize" function of this state
void GameStateAsteroidsInit(void)
{

	srand(time(NULL));

	AEGfxSetBackgroundColor(0.0f, 0.0f, 0.0f);
	AEGfxSetBlendMode(AE_GFX_BM_BLEND);

	// zero the game object instance array
	memset(sgGameObjectInstanceList, 0, sizeof(GameObjectInstance)* GAME_OBJ_INST_NUM_MAX);
	// No game object instances (sprites) at this point
	sgGameObjectInstanceNum = 0;

	// create the main ship
	sgpShip = GameObjectInstanceCreate(OBJECT_TYPE_SHIP);

	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	// TO DO 8:
	// -- Create at least 3 asteroid instances, each with a different size, 
	//    using the "GameObjInstCreate" function
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	int asteroid_count = rand() % 10;
	int i = 0;
	float xr, yr,vxr,vyr,scx,scy,right,left,top,bot;
	right = AEGfxGetWinMaxX();
	left = AEGfxGetWinMinX();
	top = AEGfxGetWinMaxY();
	bot = AEGfxGetWinMinY();
	GameObjectInstance *pInst;
	for (i = 0; i < asteroid_count; i++)
	{
		pInst = GameObjectInstanceCreate(OBJECT_TYPE_ASTEROID);
		
		if (pInst->mpComponent_Sprite->mpShape->mType == OBJECT_TYPE_ASTEROID)
		{

			xr = rand() % (int)right;
			yr = rand() % (int)top;
			vxr = rand() % 189;
			vyr = rand() % 150;
			scx = rand() % 200;
			scy = rand() % 200;
			Vector2DSet(&pInst->mpComponent_Transform->mPosition, xr, yr);
			pInst->mpComponent_Physics->mVelocity.x = vxr;
			pInst->mpComponent_Physics->mVelocity.y = vyr;
			pInst->mpComponent_Transform->mScaleX = scx;
			pInst->mpComponent_Transform->mScaleY = scy;
		}
	}
		
	/*Vector2DSet(&asteroid->mpComponent_Transform->mPosition, -34.0f, 78.0f);
	asteroid->mpComponent_Transform->mScaleX = 60.0f;
	asteroid->mpComponent_Transform->mScaleY = 60.0f;*/

	//float r = rand() % 100;
	// reset the score and the number of ship
	sgScore			= 0;
	sgShipLives		= SHIP_INITIAL_NUM;

	// INitialise ship scale
	sgpShip->mpComponent_Transform->mScaleX = 30.0f;
	sgpShip->mpComponent_Transform->mScaleY = 30.0f;
}

// ---------------------------------------------------------------------------

// HOMING MISSILE BEHAVIOUR
void AcquireTarget(GameObjectInstance* mis)
{
	float MIN =0, distance;
	for (int i = 0; i < GAME_OBJ_INST_NUM_MAX; i++)
	{
		GameObjectInstance* pInst = sgGameObjectInstanceList + i;
		// skip non-active object
		if ((pInst->mFlag & FLAG_ACTIVE) == 0)
			continue;

		if (pInst->mpComponent_Sprite->mpShape->mType == OBJECT_TYPE_ASTEROID)
		{
				mis->mpComponent_Target->mpTarget = pInst;
				break;
		}
		
	}
}

void Homing(GameObjectInstance* mis)
{
	Matrix2D rotation;
	Vector2D target_vector, normalized_target_vector,normal;
	float dist_target, cos_angle, angle, turn;
	AcquireTarget(mis);

	// target vector
	Vector2DSub(&target_vector, &mis->mpComponent_Target->mpTarget->mpComponent_Transform->mPosition,&mis->mpComponent_Transform->mPosition);
	// unit vector in dir of target
	Vector2DNormalize(&normalized_target_vector, &target_vector);
	//distance between target and missile
	Vector2DDistance(mis, mis->mpComponent_Target);
	//angle between normalized_target and missile
	cos_angle = Vector2DDotProduct(&normalized_target_vector, &mis->mpComponent_Physics->mVelocity);

	angle = acosf(cos_angle);
	//calculating normal
	Matrix2DRotRad(&rotation, HOMING_MISSILE_ROT_SPEED);
	Matrix2DMultVec(&normal, &rotation, &mis->mpComponent_Physics->mVelocity);
	// DOT product for turning
	turn = Vector2DDotProduct(&normal, &target_vector);
	if (turn>0)
	{
		if(angle<HOMING_MISSILE_ROT_SPEED)
		{
			mis->mpComponent_Transform->mAngle += angle;
		}
		else
		{
			mis->mpComponent_Transform->mAngle += HOMING_MISSILE_ROT_SPEED*frameTime;
		}
		Vector2DFromAngleRad(&mis->mpComponent_Physics->mVelocity, mis->mpComponent_Transform->mAngle);
		Vector2DScale(&mis->mpComponent_Physics->mVelocity, &mis->mpComponent_Physics->mVelocity, 160.0f);
	}
	else
	{
		if (angle<HOMING_MISSILE_ROT_SPEED)
		{
			mis->mpComponent_Transform->mAngle -= angle;
		}
		else
		{
			mis->mpComponent_Transform->mAngle -= HOMING_MISSILE_ROT_SPEED*frameTime;
		}
		Vector2DFromAngleRad(&mis->mpComponent_Physics->mVelocity, mis->mpComponent_Transform->mAngle);
		Vector2DScale(&mis->mpComponent_Physics->mVelocity, &mis->mpComponent_Physics->mVelocity, 160.0f);
	}
}
// "Update" function of this state
void GameStateAsteroidsUpdate(void)
{
	unsigned long i;
	float winMaxX, winMaxY, winMinX, winMinY;
	

	// ==========================================================================================
	// Getting the window's world edges (These changes whenever the camera moves or zooms in/out)
	// ==========================================================================================
	winMaxX = AEGfxGetWinMaxX();
	winMaxY = AEGfxGetWinMaxY();
	winMinX = AEGfxGetWinMinX();
	winMinY = AEGfxGetWinMinY();

	
	// ======================
	// Getting the frame time
	// ======================

	frameTime = AEFrameRateControllerGetFrameTime();

	// =========================
	// Update according to input
	// =========================

	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	// TO DO 3:
	// -- Compute the forward/backward acceleration of the ship when Up/Down are pressed
	// -- Use the acceleration to update the velocity of the ship
	// -- Limit the maximum velocity of the ship
	// -- IMPORTANT: The current input code moves the ship by simply adjusting its position
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	if (AEInputCheckCurr(VK_UP))
	{
		Vector2D unit_dir;
		Vector2DSet(&unit_dir, cosf(sgpShip->mpComponent_Transform->mAngle), sinf(sgpShip->mpComponent_Transform->mAngle));

		//scaling for accel
		Vector2DScale(&unit_dir, &unit_dir, (SHIP_ACCEL_FORWARD*frameTime));

		// vnew
		Vector2DAdd(&sgpShip->mpComponent_Physics->mVelocity, &sgpShip->mpComponent_Physics->mVelocity, &unit_dir);
	}

	if (AEInputCheckCurr(VK_DOWN))
	{
		Vector2D unit_dir;
		Vector2DSet(&unit_dir, cosf(sgpShip->mpComponent_Transform->mAngle), sinf(sgpShip->mpComponent_Transform->mAngle));

		//scaling for accel
		Vector2DScale(&unit_dir, &unit_dir, (SHIP_ACCEL_BACKWARD*frameTime));

		// vnew
		Vector2DAdd(&sgpShip->mpComponent_Physics->mVelocity, &sgpShip->mpComponent_Physics->mVelocity, &unit_dir);
	}

	if (AEInputCheckCurr(VK_LEFT))
	{
		sgpShip->mpComponent_Transform->mAngle += SHIP_ROT_SPEED * (float)(frameTime);
		sgpShip->mpComponent_Transform->mAngle = AEWrap(sgpShip->mpComponent_Transform->mAngle, -PI, PI);
	}

	if (AEInputCheckCurr(VK_RIGHT))
	{
		sgpShip->mpComponent_Transform->mAngle -= SHIP_ROT_SPEED * (float)(frameTime);
		sgpShip->mpComponent_Transform->mAngle = AEWrap(sgpShip->mpComponent_Transform->mAngle, -PI, PI);
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	// TO DO 5:
	// -- Create a bullet instance when SPACE is triggered, using the "GameObjInstCreate" function
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	if (AEInputCheckTriggered(VK_SPACE))
	{
		Vector2D dir;
		GameObjectInstance *bullet=GameObjectInstanceCreate(1);
		bullet->mpComponent_Transform->mScaleX = 40;
		bullet->mpComponent_Transform->mScaleX = 40;
		bullet->mpComponent_Transform->mPosition = sgpShip->mpComponent_Transform->mPosition;
		bullet->mpComponent_Transform->mAngle = sgpShip->mpComponent_Transform->mAngle;
		//bullet->mpComponent_Physics->mVelocity = sgpShip->mpComponent_Physics->mVelocity;
		//set bullet direction
		Vector2DSet(&dir, cosf(bullet->mpComponent_Transform->mAngle), sinf(bullet->mpComponent_Transform->mAngle));
	
		Vector2DScaleAdd(&bullet->mpComponent_Physics->mVelocity,&dir, &bullet->mpComponent_Physics->mVelocity,BULLET_SPEED);
		//Vector2DScale(&bullet->mpComponent_Physics->mVelocity, &bullet->mpComponent_Physics->mVelocity, BULLET_SPEED);
		

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	// TO DO 11:
	// -- Create a homing missile instance when M is triggered
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	if (AEInputCheckTriggered('M'))
	{
		Vector2D mis_dir;
		GameObjectInstance *missile = GameObjectInstanceCreate(3);
		missile->mpComponent_Transform->mScaleX = 25;
		missile->mpComponent_Transform->mScaleY = 5;
		missile->mpComponent_Transform->mPosition = sgpShip->mpComponent_Transform->mPosition;
		missile->mpComponent_Transform->mAngle = sgpShip->mpComponent_Transform->mAngle;

		Vector2DSet(&mis_dir, cosf(missile->mpComponent_Transform->mAngle), sinf(missile->mpComponent_Transform->mAngle));

		Vector2DScaleAdd(&missile->mpComponent_Physics->mVelocity, &mis_dir, &missile->mpComponent_Physics->mVelocity, 100.0f);
		AcquireTarget(missile);
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	// TO DO 2:
	// Update the positions of all active game object instances
	// -- Positions are updated here (P1 = V1*t + P0)
	// -- If implemented correctly, you will be able to control the ship (basic 2D movement)
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	for (i = 0; i < GAME_OBJ_INST_NUM_MAX; i++)
	{
		GameObjectInstance* pInst = sgGameObjectInstanceList + i;

		// skip non-active object
		if ((pInst->mFlag & FLAG_ACTIVE) == 0)
			continue;

		//update pos with vel without accel key press for all active game instances
		//AESysPrintf("Updating the position : x: %.2f , y : %.2f ,...with vel : x: %.2f , y: %.2f \n", pInst->mpComponent_Transform->mPosition.x, pInst->mpComponent_Transform->mPosition.y, pInst->mpComponent_Physics->mVelocity.x, pInst->mpComponent_Physics->mVelocity.y);
		
		//call homing
		if(pInst->mpComponent_Sprite->mpShape->mType==OBJECT_TYPE_HOMING_MISSILE)
		Homing(pInst);

		else if(pInst->mpComponent_Sprite->mpShape->mType == OBJECT_TYPE_SHIP)
		{ 
			// CAP ship speed
			Vector2DScale(&pInst->mpComponent_Physics->mVelocity, &pInst->mpComponent_Physics->mVelocity, .99);
		}

		Vector2DScaleAdd(&pInst->mpComponent_Transform->mPosition, &pInst->mpComponent_Physics->mVelocity, &pInst->mpComponent_Transform->mPosition, frameTime);
	}




	



	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	// TO DO 6: Specific game object behavior, according to type
	// -- Bullet: Destroy when it's outside the viewport
	// -- Asteroids: If it's outside the viewport, wrap around viewport.
	// -- Homing missile: If it's outside the viewport, wrap around viewport.
	// -- Homing missile: Follow/Acquire target
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	for (i = 0; i < GAME_OBJ_INST_NUM_MAX; i++)
	{
		GameObjectInstance* pInst = sgGameObjectInstanceList + i;

		// skip non-active object
		if ((pInst->mFlag & FLAG_ACTIVE) == 0)
			continue;
		
		// check if the object is a ship
		if (pInst->mpComponent_Sprite->mpShape->mType == OBJECT_TYPE_SHIP)
		{
			// warp the ship from one end of the screen to the other
			pInst->mpComponent_Transform->mPosition.x = AEWrap(pInst->mpComponent_Transform->mPosition.x, winMinX - SHIP_SIZE, winMaxX + SHIP_SIZE);
			pInst->mpComponent_Transform->mPosition.y = AEWrap(pInst->mpComponent_Transform->mPosition.y, winMinY - SHIP_SIZE, winMaxY + SHIP_SIZE);
		}

		// Bullet behavior
		  else if (pInst->mpComponent_Sprite->mpShape->mType == OBJECT_TYPE_BULLET)	
		{
			if (pInst->mpComponent_Transform->mPosition.x > winMaxX || pInst->mpComponent_Transform->mPosition.x < winMinX)
				GameObjectInstanceDestroy(pInst);

			else if (pInst->mpComponent_Transform->mPosition.y > winMaxY || pInst->mpComponent_Transform->mPosition.y < winMinY)
				GameObjectInstanceDestroy(pInst);
		}
		  
		// Asteroid behavior
		  else if (pInst->mpComponent_Sprite->mpShape->mType == OBJECT_TYPE_ASTEROID)
		  {
			  pInst->mpComponent_Transform->mPosition.x = AEWrap(pInst->mpComponent_Transform->mPosition.x, winMinX - SHIP_SIZE, winMaxX + SHIP_SIZE);
			pInst->mpComponent_Transform->mPosition.y = AEWrap(pInst->mpComponent_Transform->mPosition.y, winMinY - SHIP_SIZE, winMaxY + SHIP_SIZE);
		  }

		// Homing missile behavior (Not every game object instance will have this component!)

	}


	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	// TO DO 9: Check for collision
	// Important: Take the object instance's scale values into consideration when checking for collision.
	// -- Asteroid - Bullet: Rectangle to Point check. If they collide, destroy both.
	// -- Asteroid - Ship: Rectangle to Rectangle check. If they collide, destroy the asteroid, 
	//    reset the ship position to the center of the screen.
	// -- Asteroid - Homing Missile: Rectangle to Rectangle check.
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/*
	for each object instance: oi1
		if oi1 is not active
			skip

		if oi1 is an asteroid
			for each object instance oi2
				if(oi2 is not active or oi2 is an asteroid)
					skip

				if(oi2 is the ship)
					Check for collision between the ship and the asteroid
					Update game behavior accordingly
					Update "Object instances array"
				else
				if(oi2 is a bullet)
					Check for collision between the bullet and the asteroid
					Update game behavior accordingly
					Update "Object instances array"
				else
				if(oi2 is a missle)
					Check for collision between the missile and the asteroid
					Update game behavior accordingly
					Update "Object instances array"
	*/
	
	int j;
	for (i = 0; i < GAME_OBJ_INST_NUM_MAX; i++)
	{
		GameObjectInstance* oi2; 
		GameObjectInstance* pInst = sgGameObjectInstanceList + i;
		// skip non-active object
		if ((pInst->mFlag & FLAG_ACTIVE) == 0)
			continue;
		
		else if (pInst->mpComponent_Sprite->mpShape->mType == OBJECT_TYPE_ASTEROID)
		{
			//oi2 = pInst;
			for (j = 0; j < GAME_OBJ_INST_NUM_MAX; j++)
			{
				oi2 = sgGameObjectInstanceList + j;

				if (((oi2->mFlag & FLAG_ACTIVE) == 0)  ||  oi2->mpComponent_Sprite->mpShape->mType == OBJECT_TYPE_ASTEROID)
					continue;

				else if (oi2->mpComponent_Sprite->mpShape->mType == OBJECT_TYPE_SHIP)
				{
					if (StaticRectToStaticRect(&oi2->mpComponent_Transform->mPosition, oi2->mpComponent_Transform->mScaleX/10.0f, oi2->mpComponent_Transform->mScaleY/10.0f, &pInst->mpComponent_Transform->mPosition, pInst->mpComponent_Transform->mScaleX, pInst->mpComponent_Transform->mScaleY)==1)
					{
						GameObjectInstanceDestroy(pInst);
						Asteroid_create();
						if (sgShipLives < 0)
							AESysPrintf("GAME OVER");
						else
							sgShipLives--;
						
						Vector2DSet(&sgpShip->mpComponent_Transform->mPosition,0.0f,0.0f);
						break;
					}
				 }
				else if (oi2->mpComponent_Sprite->mpShape->mType == OBJECT_TYPE_BULLET)
				{
					if (StaticPointToStaticRect(&oi2->mpComponent_Transform->mPosition, &pInst->mpComponent_Transform->mPosition, pInst->mpComponent_Transform->mScaleX, pInst->mpComponent_Transform->mScaleY) == 1)
					{
						GameObjectInstanceDestroy(pInst);
						GameObjectInstanceDestroy(oi2);
						Asteroid_create();
						break;
					}
				}
				else if (oi2->mpComponent_Sprite->mpShape->mType == OBJECT_TYPE_HOMING_MISSILE)
				{
					 if (StaticRectToStaticRect(&oi2->mpComponent_Transform->mPosition, oi2->mpComponent_Transform->mScaleX / 10.0f, oi2->mpComponent_Transform->mScaleY / 10.0f, &pInst->mpComponent_Transform->mPosition, pInst->mpComponent_Transform->mScaleX, pInst->mpComponent_Transform->mScaleY) == 1)
					{
						GameObjectInstanceDestroy(pInst);
						GameObjectInstanceDestroy(oi2);
						Asteroid_create();
						break;
					}
				}
			}
		}
	}

	// =====================================
	// calculate the matrix for all objects
	// =====================================

	for (i = 0; i < GAME_OBJ_INST_NUM_MAX; i++)
	{
		Matrix2D		 trans, rot, scale;
		GameObjectInstance* pInst = sgGameObjectInstanceList + i;
		
		// skip non-active object
		if ((pInst->mFlag & FLAG_ACTIVE) == 0)
			continue;


		/////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////
		// TO DO 1:
		// -- Build the transformation matrix of each active game object instance
		// -- After you implement this step, you should see the player's ship
		// -- Reminder: Scale should be applied first, then rotation, then translation.
		/////////////////////////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////////////////////////


		// Compute the scaling matrix
		Matrix2DScale(&scale,pInst->mpComponent_Transform->mScaleX, pInst->mpComponent_Transform->mScaleX);
		// Compute the rotation matrix 
		Matrix2DRotRad(&rot,pInst->mpComponent_Transform->mAngle);
		// Compute the translation matrix
		Matrix2DTranslate(&trans,pInst->mpComponent_Transform->mPosition.x, pInst->mpComponent_Transform->mPosition.y);
		// Concatenate the 3 matrix in the correct order in the object instance's transform component's "mTransform" matrix
		Matrix2DConcat(&rot, &rot, &scale);
		Matrix2DConcat(&pInst->mpComponent_Transform->mTransform, &trans, &rot);
	}
}

// ---------------------------------------------------------------------------

void GameStateAsteroidsDraw(void)
{
	unsigned long i;
	double frameTime;

	AEGfxSetRenderMode(AE_GFX_RM_COLOR);
	AEGfxTextureSet(NULL, 0, 0);
	AEGfxSetTintColor(1.0f, 1.0f, 1.0f, 1.0f);

	// draw all object instances in the list

	for (i = 0; i < GAME_OBJ_INST_NUM_MAX; i++)
	{
		GameObjectInstance* pInst = sgGameObjectInstanceList + i;

		// skip non-active object
		if ((pInst->mFlag & FLAG_ACTIVE) == 0)
			continue;
		
		// Already implemented. Explanation:
		// Step 1 & 2 are done outside the for loop (AEGfxSetRenderMode, AEGfxTextureSet, AEGfxSetTintColor) since all our objects share the same material.
		// If you want to have objects with difference materials (Some with textures, some without, some with transparency etc...)
		// then you'll need to move those functions calls inside the for loop
		// 1 - Set Render Mode (Color or texture)
		// 2 - Set all needed parameters (Color blend, textures, etc..)
		// 3 - Set the current object instance's mTransform matrix using "AEGfxSetTransform"
		// 4 - Draw the shape used by the current object instance using "AEGfxMeshDraw"

		AEGfxSetTransform(pInst->mpComponent_Transform->mTransform.m);
		AEGfxMeshDraw(pInst->mpComponent_Sprite->mpShape->mpMesh, AE_GFX_MDM_TRIANGLES);
	}
}

// ---------------------------------------------------------------------------

void GameStateAsteroidsFree(void)
{
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	// TO DO 12:
	//  -- Destroy all the active game object instances, using the “GameObjInstanceDestroy” function.
	//  -- Reset the number of active game objects instances
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	
	for (int i = 0; i < GAME_OBJ_INST_NUM_MAX; i++)
	{
		GameObjectInstance* pInst = sgGameObjectInstanceList + i;
			GameObjectInstanceDestroy(pInst);
			sgGameObjectInstanceNum--;
	}
}

// ---------------------------------------------------------------------------

void GameStateAsteroidsUnload(void)
{
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	// TO DO 13:
	//  -- Destroy all the shapes, using the “AEGfxMeshFree” function.
	/////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////
	Shape* pshape;
	for (int i = 0; i < sgShapeNum; i++)
	{
		pshape = sgShapes + i;
		AEGfxMeshFree(pshape->mpMesh);
	}
}

// ---------------------------------------------------------------------------

GameObjectInstance* GameObjectInstanceCreate(unsigned int ObjectType)			// From OBJECT_TYPE enum)
{
	unsigned long i;
	
	// loop through the object instance list to find a non-used object instance
	for (i = 0; i < GAME_OBJ_INST_NUM_MAX; i++)
	{
		GameObjectInstance* pInst = sgGameObjectInstanceList + i;

		// Check if current instance is not used
		if (pInst->mFlag == 0)
		{
			// It is not used => use it to create the new instance

			// Active the game object instance
			pInst->mFlag = FLAG_ACTIVE;

			pInst->mpComponent_Transform = 0;
			pInst->mpComponent_Sprite = 0;
			pInst->mpComponent_Physics = 0;
			pInst->mpComponent_Target = 0;

			// Add the components, based on the object type
			switch (ObjectType)
			{
			case OBJECT_TYPE_SHIP:
				AddComponent_Sprite(pInst, OBJECT_TYPE_SHIP);
				AddComponent_Transform(pInst, 0, 0.0f, 1.0f, 1.0f);
				AddComponent_Physics(pInst, 0);
				break;

			case OBJECT_TYPE_BULLET:
				AddComponent_Sprite(pInst, OBJECT_TYPE_BULLET);
				AddComponent_Transform(pInst, 0, 0.0f, 1.0f, 1.0f);
				AddComponent_Physics(pInst, 0);
				break;

			case OBJECT_TYPE_ASTEROID:
				AddComponent_Sprite(pInst, OBJECT_TYPE_ASTEROID);
				AddComponent_Transform(pInst, 0, 0.0f, 1.0f, 1.0f);
				AddComponent_Physics(pInst, 0);
				break;

			case OBJECT_TYPE_HOMING_MISSILE:
				AddComponent_Sprite(pInst, OBJECT_TYPE_HOMING_MISSILE);
				AddComponent_Transform(pInst, 0, 0.0f, 1.0f, 1.0f);
				AddComponent_Physics(pInst, 0);
				AddComponent_Target(pInst, 0);
				break;
			}

			++sgGameObjectInstanceNum;

			// return the newly created instance
			return pInst;
		}
	}

	// Cannot find empty slot => return 0
	return 0;
}

// ---------------------------------------------------------------------------

void GameObjectInstanceDestroy(GameObjectInstance* pInst)
{
	// if instance is destroyed before, just return
	if (pInst->mFlag == 0)
		return;

	// Zero out the mFlag
	pInst->mFlag = 0;

	RemoveComponent_Transform(pInst);
	RemoveComponent_Sprite(pInst);
	RemoveComponent_Physics(pInst);
	RemoveComponent_Target(pInst);

	--sgGameObjectInstanceNum;
}

// ---------------------------------------------------------------------------

void AddComponent_Transform(GameObjectInstance *pInst, Vector2D *pPosition, float Angle, float ScaleX, float ScaleY)
{
	if (0 != pInst)
	{
		if (0 == pInst->mpComponent_Transform)
		{
			pInst->mpComponent_Transform = (Component_Transform *)calloc(1, sizeof(Component_Transform));
		}

		Vector2D zeroVec2;
		Vector2DZero(&zeroVec2);

		pInst->mpComponent_Transform->mScaleX = ScaleX;
		pInst->mpComponent_Transform->mScaleY = ScaleY;
		pInst->mpComponent_Transform->mPosition = pPosition ? *pPosition : zeroVec2;;
		pInst->mpComponent_Transform->mAngle = Angle;
		pInst->mpComponent_Transform->mpOwner = pInst;
	}
}

// ---------------------------------------------------------------------------

void AddComponent_Sprite(GameObjectInstance *pInst, unsigned int ShapeType)
{
	if (0 != pInst)
	{
		if (0 == pInst->mpComponent_Sprite)
		{
			pInst->mpComponent_Sprite = (Component_Sprite *)calloc(1, sizeof(Component_Sprite));
		}
	
		pInst->mpComponent_Sprite->mpShape = sgShapes + ShapeType;
		pInst->mpComponent_Sprite->mpOwner = pInst;
	}
}

// ---------------------------------------------------------------------------

void AddComponent_Physics(GameObjectInstance *pInst, Vector2D *pVelocity)
{
	if (0 != pInst)
	{
		if (0 == pInst->mpComponent_Physics)
		{
			pInst->mpComponent_Physics = (Component_Physics *)calloc(1, sizeof(Component_Physics));
		}

		Vector2D zeroVec2;
		Vector2DZero(&zeroVec2);

		pInst->mpComponent_Physics->mVelocity = pVelocity ? *pVelocity : zeroVec2;
		pInst->mpComponent_Physics->mpOwner = pInst;
	}
}

// ---------------------------------------------------------------------------

void AddComponent_Target(GameObjectInstance *pInst, GameObjectInstance *pTarget)
{
	if (0 != pInst)
	{
		if (0 == pInst->mpComponent_Target)
		{
			pInst->mpComponent_Target = (Component_Target *)calloc(1, sizeof(Component_Target));
		}

		pInst->mpComponent_Target->mpTarget = pTarget;
		pInst->mpComponent_Target->mpOwner = pInst;
	}
}

// ---------------------------------------------------------------------------

void RemoveComponent_Transform(GameObjectInstance *pInst)
{
	if (0 != pInst)
	{
		if (0 != pInst->mpComponent_Transform)
		{
			free(pInst->mpComponent_Transform);
			pInst->mpComponent_Transform = 0;
		}
	}
}

// ---------------------------------------------------------------------------

void RemoveComponent_Sprite(GameObjectInstance *pInst)
{
	if (0 != pInst)
	{
		if (0 != pInst->mpComponent_Sprite)
		{
			free(pInst->mpComponent_Sprite);
			pInst->mpComponent_Sprite = 0;
		}
	}
}

// ---------------------------------------------------------------------------

void RemoveComponent_Physics(GameObjectInstance *pInst)
{
	if (0 != pInst)
	{
		if (0 != pInst->mpComponent_Physics)
		{
			free(pInst->mpComponent_Physics);
			pInst->mpComponent_Physics = 0;
		}
	}
}

// ---------------------------------------------------------------------------

void RemoveComponent_Target(GameObjectInstance *pInst)
{
	if (0 != pInst)
	{
		if (0 != pInst->mpComponent_Target)
		{
			free(pInst->mpComponent_Target);
			pInst->mpComponent_Target = 0;
		}
	}
}
