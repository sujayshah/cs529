/*!****************************************************************************
  \file   GameState_Demo.h

  \date   7 March 2016

  \author Brandon Hare

  \par    Course
            CS230

  \par    Project
            Interactive Reflection Demo

  \par    DigiPen Login
            brandon.hare

  \par    DigiPen Email
            brandon.hare\@digipen.edu

  \brief  The header file for the Test game state.  Defines the functions
            used by the game state manager.
******************************************************************************/

#ifndef _GAME_STATE_TEST_H_
#define _GAME_STATE_TEST_H_

void GameStateDemoLoad(void);
void GameStateDemoInit(void);
void GameStateDemoUpdate(void);
void GameStateDemoDraw(void);
void GameStateDemoFree(void);
void GameStateDemoUnload(void);

#endif /* _GAME_STATE_TEST_H_ */
