#include "Math2D.h"
#include "stdio.h"
#include "LineSegment2D.h"
#include "AEEngine.h"

//////////////////////
// New to project 2 //
//////////////////////

/*
This function determines the distance separating a point from a line

- Parameters
- P:		The point whose location should be determined
- LS:		The line segment

- Returned value: The distance. Note that the returned value should be:
- Negative if the point is in the line's inside half plane
- Positive if the point is in the line's outside half plane
- Zero if the point is on the line
*/
float StaticPointToStaticLineSegment(Vector2D *P, LineSegment2D *LS)
{
	float col;
	//Vector2D edge;
	//Vector2DSub(&edge, P, &LS->mP0);
	col = Vector2DDotProduct(&LS->mN, P/*&edge*/);
	//return col;

	return Vector2DDotProduct(&LS->mN, P) - LS->mNdotP0;
}


/*
This function checks whether an animated point is colliding with a line segment

- Parameters
- Ps:		The point's starting location
- Pe:		The point's ending location
- LS:		The line segment
- Pi:		This will be used to store the intersection point's coordinates (In case there's an intersection)

- Returned value: Intersection time t
- -1.0f:				If there's no intersection
- Intersection time:	If there's an intersection
*/
float AnimatedPointToStaticLineSegment(Vector2D *Ps, Vector2D *Pe, LineSegment2D *LS, Vector2D *Pi)
{
	//int BuildLineSegment2D(LineSegment2D *LS, Vector2D *Point0, Vector2D *Point1);
	Vector2D v, Bi_P0, edge, Bi_P1, P0_P1;
	float n_dot_Bs, n_dot_v, t, q, n_dot_Be;
	// compute v
	Vector2DSub(&v, Pe, Ps);
	//n.Bs 
	n_dot_Bs = Vector2DDotProduct(&LS->mN, Ps);
	//n.v
	n_dot_v = Vector2DDotProduct(&LS->mN, &v);

	//n. Be
	n_dot_Be = Vector2DDotProduct(&LS->mN, Pe);


	if ((n_dot_Bs<LS->mNdotP0) && (n_dot_Be<LS->mNdotP0))
		return -1.0f;
	else if ((n_dot_Bs>LS->mNdotP0) && (n_dot_Be>LS->mNdotP0))
		return -1.0f;
	else if (n_dot_v == 0)
		return -1.0f;


	t = (LS->mNdotP0 - n_dot_Bs) / n_dot_v;

	//AESysPrintf("t: %f\n", t);
	//v*t
	//Vector2DScale(&v, &v, t);
	// Bi=Bs+v*t
	Vector2DScaleAdd(Pi, &v, Ps, t);

	//Bi-P0
	Vector2DSub(&Bi_P0, Pi, &LS->mP0);

	//p1-p0
	Vector2DSub(&edge, &LS->mP1, &LS->mP0);

	//Bi-p1
	Vector2DSub(&Bi_P1, Pi, &LS->mP1);

	//p0-p1
	Vector2DSub(&P0_P1, &LS->mP0, &LS->mP1);


	if (Vector2DDotProduct(&Bi_P0, &edge)<0)
		return -1.0f;
	else if (Vector2DDotProduct(&Bi_P1, &P0_P1)<0)
		return -1.0f;
	else if (t > 0 || t < 1)
		return t;


	return -1.0f;
}


/*
This function checks whether an animated circle is colliding with a line segment

- Parameters
- Ps:		The center's starting location
- Pe:		The center's ending location
- Radius:	The circle's radius
- LS:		The line segment
- Pi:		This will be used to store the intersection point's coordinates (In case there's an intersection)

- Returned value: Intersection time t
- -1.0f:				If there's no intersection
- Intersection time:	If there's an intersection
*/
float AnimatedCircleToStaticLineSegment(Vector2D *Ps, Vector2D *Pe, float Radius, LineSegment2D *LS, Vector2D *Pi)
{
	Vector2D v, Bi_P0, edge, Bi_P1, P0_P1;
	float n_dot_Bs, n_dot_v, t, q, n_dot_Be;
	// compute v
	Vector2DSub(&v, Pe, Ps);
	//n.Bs 
	n_dot_Bs = Vector2DDotProduct(&LS->mN, Ps);
	//n.v
	n_dot_v = Vector2DDotProduct(&LS->mN, &v);


	if (StaticPointToStaticLineSegment(Ps, LS) < 0) {

		Radius = Radius * -1;
	}


	t = (LS->mNdotP0 - n_dot_Bs + Radius) / n_dot_v;
	//AESysPrintf("t: %f\tn_dot_Bs: %f\t mNdotP0: %f\n", t, n_dot_Bs, LS->mNdotP0);
	//v*t
	//Vector2DScale(&v, &v, t);
	// Bi=Bs+v*t
	Vector2DScaleAdd(Pi, &v, Ps, t);

	//n. Be
	n_dot_Be = Vector2DDotProduct(&LS->mN, Pe);


	//Bi-P0
	Vector2DSub(&Bi_P0, Pi, &LS->mP0);

	//p1-p0
	Vector2DSub(&edge, &LS->mP1, &LS->mP0);

	//Bi-p1
	Vector2DSub(&Bi_P1, Pi, &LS->mP1);

	//p0-p1
	Vector2DSub(&P0_P1, &LS->mP0, &LS->mP1);

	if ((n_dot_Bs - LS->mNdotP0<Radius) && (n_dot_Be - LS->mNdotP0<Radius))
		return -1.0f;
	else if ((n_dot_Bs - LS->mNdotP0>Radius) && (n_dot_Be - LS->mNdotP0)>Radius)
		return -1.0f;
	else if (Vector2DDotProduct(&Bi_P0, &edge)<0)
		return -1.0f;
	else if (Vector2DDotProduct(&Bi_P1, &P0_P1)<0)
		return -1.0f;
	else
		return t;
}


/*
This function reflects an animated point on a line segment.
It should first make sure that the animated point is intersecting with the line

- Parameters
- Ps:		The point's starting location
- Pe:		The point's ending location
- LS:		The line segment
- Pi:		This will be used to store the intersection point's coordinates (In case there's an intersection)
- R:		Reflected vector R

- Returned value: Intersection time t
- -1.0f:				If there's no intersection
- Intersection time:	If there's an intersection
*/
float ReflectAnimatedPointOnStaticLineSegment(Vector2D *Ps, Vector2D *Pe, LineSegment2D *LS, Vector2D *Pi, Vector2D *R)
{
	Vector2D i, s, n = LS->mN;
	float t, sval;
	t = AnimatedPointToStaticLineSegment(Ps, Pe, LS, Pi);

	if (t == -1.0f) {

		return t;
	}

	//i=Be-Bi
	Vector2DSub(&i, Pe, Pi);

	//2s=2(i.n)n
	sval = Vector2DDotProduct(&i, &LS->mN);

	Vector2DScale(&s, &n, 2 * sval);


	//R=i-2s
	Vector2DSub(R, &i, &s);


	return t;
}


/*
This function reflects an animated circle on a line segment.
It should first make sure that the animated point is intersecting with the line

- Parameters
- Ps:		The center's starting location
- Pe:		The center's ending location
- Radius:	The circle's radius
- LS:		The line segment
- Pi:		This will be used to store the intersection point's coordinates (In case there's an intersection)
- R:		Reflected vector R

- Returned value: Intersection time t
- -1.0f:				If there's no intersection
- Intersection time:	If there's an intersection
*/
float ReflectAnimatedCircleOnStaticLineSegment(Vector2D *Ps, Vector2D *Pe, float Radius, LineSegment2D *LS, Vector2D *Pi, Vector2D *R)
{
	//return AnimatedCircleToStaticLineSegment(Ps, Pe, Radius, LS, Pi);


	Vector2D i, s, r;
	float t, sval;
	t = AnimatedCircleToStaticLineSegment(Ps, Pe, Radius, LS, Pi);

	if (t == -1.0f) {

		return t;
	}

	//i=Be-Bi
	Vector2DSub(&i, Pe, Pi);

	//2s=2(i.n)n
	sval = Vector2DDotProduct(&i, &LS->mN);
	sval = 2 * sval;
	Vector2DScale(&s, &LS->mN, sval);


	Vector2DSub(R, &i, &s);

	//R=i-2s

	return t;
}


/*
This function checks whether an animated point is colliding with a static circle

- Parameters
- Ps:		The point's starting location
- Pe:		The point's ending location
- Center:	The circle's center
- Radius:	The circle's radius
- Pi:		This will be used to store the intersection point's coordinates (In case there's an intersection)

- Returned value: Intersection time t
- -1.0f:		If there's no intersection
- Intersection time:	If there's an intersection
*/
float AnimatedPointToStaticCircle(Vector2D *Ps, Vector2D *Pe, Vector2D *Center, float Radius, Vector2D *Pi)
{
	Vector2D v, C_Ps, normalized_v;
	float mag_v, m, hyp, n2, t, s2, a, b, c;
	Vector2DSub(&v, Pe, Ps);
	mag_v = Vector2DLength(&v);
	Vector2DNormalize(&normalized_v, &v);
	Vector2DSub(&C_Ps, Center, Ps);
	/*m val*/
	m = Vector2DDotProduct(&normalized_v, &C_Ps);
	/*hypo Ps_C*/
	hyp = Vector2DLength(&C_Ps);
	n2 = hyp*hyp - m*m;

	/*calculating s*/
	s2 = Radius*Radius - n2;
	/* ray is intersecting the circle*/

	/*a = Vector2DDotProduct(&v, &v);
	b = -2 * Vector2DDotProduct(&C_Ps,&v);
	c = Vector2DDotProduct(&C_Ps,&C_Ps);*/
	if (n2 > Radius*Radius)
	{
		return -1.0f;
	}
	else if (m < 0 && Vector2DSquareDistance(Ps, Center)>Radius*Radius)
	{
		return -1.0f;
	}
	else
	{
		t = (m - sqrt(s2)) / mag_v;
		Vector2DScaleAdd(Pi, &v, Ps, t);
		return t;
	}

}



/*
This function reflects an animated point on a static circle.
It should first make sure that the animated point is intersecting with the circle

- Parameters
- Ps:		The point's starting location
- Pe:		The point's ending location
- Center:	The circle's center
- Radius:	The circle's radius
- Pi:		This will be used to store the intersection point's coordinates (In case there's an intersection)
- R:		Reflected vector R

- Returned value: Intersection time t
- -1.0f:		If there's no intersection
- Intersection time:	If there's an intersection
*/
float ReflectAnimatedPointOnStaticCircle(Vector2D *Ps, Vector2D *Pe, Vector2D *Center, float Radius, Vector2D *Pi, Vector2D *R)
{

	Vector2D v, r;
	float t, k;
	/*original vector length*/
	Vector2DSub(&v, Pe, Ps);
	t = AnimatedPointToStaticCircle(Ps, Pe, Center, Radius, Pi);
	//Vector2DScaleAdd(Pi, &v, Ps, t);
	if (t == -1.0f)
		return t;
	else if (t > 1.0f || t < 0.0f)
		return -1.0f;
	else
	{

		Vector2D m, n;

		/*m=C->Bi*/
		Vector2DSub(&m, Pi, Ps);

		/*n*/
		Vector2DSub(&n, Center, Pi);
		/*normalize n*/
		Vector2DNormalize(&n, &n);

		/*compute m=Bs-Bi*/
		Vector2DSub(&m, Ps, Pi);

		/*r=2(m.n)n-m*/
		Vector2DScale(&n, &n, 2 * Vector2DDotProduct(&m, &n));
		Vector2DSub(&r, &n, &m);
		Vector2DNormalize(&r, &r);

		/*Vector length v*/
		k = Vector2DLength(&v);

		Vector2DScale(R, &r, k*(1 - t));
		//Vector2DAdd(R,Pi,&r);
		return t;

	}
}


/*
This function checks whether an animated circle is colliding with a static circle

- Parameters
- Center0s:		The starting position of the animated circle's center
- Center0e:		The ending position of the animated circle's center
- Radius0:		The animated circle's radius
- Center1:		The static circle's center
- Radius1:		The static circle's radius
- Pi:			This will be used to store the intersection point's coordinates (In case there's an intersection)

- Returned value: Intersection time t
- -1.0f:		If there's no intersection
- Intersection time:	If there's an intersection
*/
float AnimatedCircleToStaticCircle(Vector2D *Center0s, Vector2D *Center0e, float Radius0, Vector2D *Center1, float Radius1, Vector2D *Pi)
{
	return AnimatedPointToStaticCircle(Center0s, Center0e, Center1, Radius0 + Radius1, Pi);
}


/*
This function reflects an animated circle on a static circle.
It should first make sure that the animated circle is intersecting with the static one

- Parameters
- Center0s:		The starting position of the animated circle's center
- Center0e:		The ending position of the animated circle's center
- Radius0:		The animated circle's radius
- Center1:		The static circle's center
- Radius1:		The static circle's radius
- Pi:			This will be used to store the intersection point's coordinates (In case there's an intersection)
- R:			Reflected vector R

- Returned value: Intersection time t
- -1.0f:		If there's no intersection
- Intersection time:	If there's an intersection
*/
float ReflectAnimatedCircleOnStaticCircle(Vector2D *Center0s, Vector2D *Center0e, float Radius0, Vector2D *Center1, float Radius1, Vector2D *Pi, Vector2D *R)
{
	//return -1.0f;
	return ReflectAnimatedPointOnStaticCircle(Center0s, Center0e, Center1, Radius0 + Radius1, Pi, R);
}

/*
This function checks if the point P is colliding with the circle whose
center is "Center" and radius is "Radius"
*/
int StaticPointToStaticCircle(Vector2D *pP, Vector2D *pCenter, float Radius)
{
	if (Radius*Radius < (pP->x - pCenter->x)*(pP->x - pCenter->x) + (pP->y - pCenter->y)*(pP->y - pCenter->y))
	{
		return 0;
	}
	else
		return 1;
}


/*
This function checks if the point Pos is colliding with the rectangle
whose center is Rect, width is "Width" and height is Height
*/
int StaticPointToStaticRect(Vector2D *pPos, Vector2D *pRect, float Width, float Height)
{
	if ((pPos->x) < (pRect->x - Width / 2) || ((pPos->x) > (pRect->x + Width / 2)) || ((pPos->y) < (pRect->y - Height / 2)) || ((pPos->y) > (pRect->y + Width / 2)))
		return 0;
	else
		return 1;

}

/*
This function checks for collision between 2 circles.
Circle0: Center is Center0, radius is "Radius0"
Circle1: Center is Center1, radius is "Radius1"
*/
int StaticCircleToStaticCircle(Vector2D *pCenter0, float Radius0, Vector2D *pCenter1, float Radius1)
{
	if ((Radius0*Radius0 + Radius1*Radius1 + 2 * Radius0*Radius1) < (pCenter0->x - pCenter1->x)*(pCenter0->x - pCenter1->x) + (pCenter0->y - pCenter1->y)*(pCenter0->y - pCenter1->y))
		return 0;
	else
		return 1;
}

/*
This functions checks if 2 rectangles are colliding
Rectangle0: Center is pRect0, width is "Width0" and height is "Height0"
Rectangle1: Center is pRect1, width is "Width1" and height is "Height1"
*/
int StaticRectToStaticRect(Vector2D *pRect0, float Width0, float Height0, Vector2D *pRect1, float Width1, float Height1)
{
	if ((pRect0->x + Width0 / 2) < (pRect1->x - Width1 / 2) || (pRect0->x - Width0 / 2) > (pRect1->x + Width1 / 2) ||
		(pRect0->y + Height0 / 2) < (pRect1->y - Height1 / 2) || (pRect0->y - Height0 / 2) > (pRect1->y + Height1 / 2))
		return 0;
	else
		return 1;
}



