/* Start Header -------------------------------------------------------
Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Vector2D.h
Purpose: skeleton for 2D Vector library
Language: C/C++ using Visual Studio 2015
Platform: Visual Studio 2015,
Hardware Requirements: 	1.6 GHz or faster processor
1 GB of RAM (1.5 GB if running on a virtual machine)
4 GB of available hard disk space 5400 RPM hard disk drive
DirectX 9-capable video card (1024 x 768 or higher resolution)
Operating Systems:	Windows 10
Windows 8.1
Windows 8
Windows 7 SP 1
Windows Server 2012 R2
Windows Server 2012
Windows Server 2008 R2 SP1

Project: 		CS529_sujay.shah_2

Author: Name: Sujay Shah ;  Login: sujay.shah  ; Student ID : 60001517
Creation date: October 5, 2017.
- End Header --------------------------------------------------------*/
#ifndef VECTOR2_H
#define VECTOR2_H

#include "math.h"



typedef struct Vector2D
{
	float x, y;
}Vector2D;

////////////////////////
// From Project 2 & 3 //
////////////////////////


void Vector2DZero(Vector2D *pResult);

void Vector2DSet(Vector2D *pResult, float x, float y);

void Vector2DNeg(Vector2D *pResult, Vector2D *pVec0);

void Vector2DAdd(Vector2D *pResult, Vector2D *pVec0, Vector2D *pVec1);

void Vector2DSub(Vector2D *pResult, Vector2D *pVec0, Vector2D *pVec1);

void Vector2DNormalize(Vector2D *pResult, Vector2D *pVec0);

void Vector2DScale(Vector2D *pResult, Vector2D *pVec0, float c);

void Vector2DScaleAdd(Vector2D *pResult, Vector2D *pVec0, Vector2D *pVec1, float c);

void Vector2DScaleSub(Vector2D *pResult, Vector2D *pVec0, Vector2D *pVec1, float c);

float Vector2DLength(Vector2D *pVec0);

float Vector2DSquareLength(Vector2D *pVec0);

float Vector2DDistance(Vector2D *pVec0, Vector2D *pVec1);

float Vector2DSquareDistance(Vector2D *pVec0, Vector2D *pVec1);

float Vector2DDotProduct(Vector2D *pVec0, Vector2D *pVec1);

void Vector2DFromAngle(Vector2D *pResult, float angle);

float Vector2DAngleFromVec2(Vector2D *pVec0);


#endif