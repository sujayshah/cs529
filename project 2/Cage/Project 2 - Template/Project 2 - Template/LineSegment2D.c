/* Start Header -------------------------------------------------------
Copyright (C) 2017 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: LineSegment2D.c
Purpose: Implementation of 2D LineSegment library
Language: C/C++ using Visual Studio 2015
Platform: Visual Studio 2015,
Hardware Requirements: 	1.6 GHz or faster processor
1 GB of RAM (1.5 GB if running on a virtual machine)
4 GB of available hard disk space 5400 RPM hard disk drive
DirectX 9-capable video card (1024 x 768 or higher resolution)
Operating Systems:	Windows 10
Windows 8.1
Windows 8
Windows 7 SP 1
Windows Server 2012 R2
Windows Server 2012
Windows Server 2008 R2 SP1

Project: 		CS529_sujay.shah_2

Author: Name: Sujay Shah ;  Login: sujay.shah  ; Student ID : 60001517
Creation date: October 5, 2017.
- End Header --------------------------------------------------------*/
#include "LineSegment2D.h"
#include "Matrix2D.h"


int BuildLineSegment2D(LineSegment2D *LS, Vector2D *Point0, Vector2D *Point1)
{
	
	Vector2D edge,normal,normalized_normal;
	Matrix2D r;
	float dot;
	Vector2DSub(&edge,Point1,Point0);
/*	if (edge.x == 0 && edge.y == 0)
		return 0;*/

	Vector2DSet(&normal, edge.y, -edge.x);
	
	Vector2DNormalize(&LS->mN, &normal);
	dot=Vector2DDotProduct(&LS->mN,Point0);

	Vector2DSet(&LS->mP0, Point0->x, Point0->y);
	Vector2DSet(&LS->mP1, Point1->x, Point1->y);
	/*LS->mP0 = &Point0;
	LS->mP1 = *Point1;*/
	LS->mNdotP0 = dot;
//	LS->mN = normalized_normal;
	
	return 1;
}