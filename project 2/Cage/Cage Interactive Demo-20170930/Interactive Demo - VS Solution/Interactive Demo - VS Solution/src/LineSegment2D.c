#include "LineSegment2D.h"
#include "Matrix2D.h"


int BuildLineSegment2D(LineSegment2D *LS, Vector2D *Point0, Vector2D *Point1)
{
	
	Vector2D edge,normal,normalized_normal;
	Matrix2D r;
	float dot;
	Vector2DSub(&edge,Point0,Point1);
	if (edge.x == 0 && edge.y == 0)
		return 0;

	Matrix2DRotDeg(&r, -90.0f);
	Matrix2DMultVec(&normal,&r,&edge);
	Vector2DNormalize(&normalized_normal,&normal);
	dot=Vector2DDotProduct(&normal,Point0);
	LS->mP0 = *Point0;
	LS->mP1 = *Point1;
	LS->mNdotP0 = dot;
	LS->mN = normalized_normal;
	
	return 1;
}