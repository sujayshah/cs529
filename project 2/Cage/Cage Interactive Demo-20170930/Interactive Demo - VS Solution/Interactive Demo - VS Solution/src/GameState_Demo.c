/*!****************************************************************************
\file   GameState_Test.c

\date   7 March 2016

\author Brandon Hare

\par    Course
CS230

\par    Project
Interactive Reflection Demo

\par    DigiPen Login
brandon.hare

\par    DigiPen Email
brandon.hare\@digipen.edu

\brief  This file implements the Test gamestate, allowing your
ReflectAnimatedPointOnStaticLineSegment function to be tested
interactively.


\par  How to use the demo
+ Click and drag to move points
(large circles at the endpoints of the lines).
+ The red line represents the wall (reflective surface).
+ The green line represents the moving point
(i.e. the velocity from the start point to the end point).
+ If the green line intersects the red line, it will reflect off, creating
the non-interactive blue reflection line.
+ Press Q to quit.
+ Press R to reset.


\par  Notes
The normals will be shown at the centre of most of the lines.
If the normal is not normalized, the displayed normal will probably
extend off-screen.
******************************************************************************/


/* AEGfxVertexList,                                                          */
/* NULL, TRUE, FALSE, TWO_PI, AE_GFX_MDM_LINES, AE_GFX_MDM_TRIANGLES,        */
/* AE_GFX_RM_COLOR, VK_LBUTTON, AE_ASSERT_MESG AEGfxMeshStart, AEGfxTriAdd,  */
/* AEGfxVertexAdd, AEGfxMeshEnd, AEGfxMeshFree, AEGfxSetBackgroundColor,     */
/* AEGfxSetTintColor, AEGfxSetRenderMode, AEGfxSetTransform, AEGfxMeshDraw,  */
/* AEGfxGetWinMinX, AEGfxGetWinMaxX, AEGfxGetWinMinY, AEGfxGetWinMaxY,       */
/* AEInputGetCursorPosition, AEInputCheckPrev, AEInputCheckTriggered,        */
/* AEInputCheckCurr                              (why did I write all this?) */
#include "AEEngine.h"
#include "GameStateMgr.h"  /* gGameStateNext, GS_RESTART, GS_QUIT            */
#include "Vector2D.h"      /* Vector2D, Vector2DAdd, Vector2DDistance        */
#include "Matrix2D.h"      /* Matrix2D, Matrix2DTranslate, Matrix2DRotRad    */
/* Matrix2DScale, Matrix2DConcat                  */
#include "LineSegment2D.h" /* LineSegment2D, BuildLineSegment2D              */
#include "Math2D.h"        /* ReflectAnimatedPointOnStaticLineSegment        */


/*********************************** MACROS **********************************/

/*! The maximum number of points */
#define MAX_POINTS   256
/*! The maximum number of lines */
#define MAX_LINES    256
/*! The maximum number of lines */
#define MAX_CIRCLES  32

/*! The number of sides for the circle polygon */
#define CIRCLE_SIDES 48

/*! The radius of a draggable point     */
#define POINT_RADIUS_DRAGGABLE   6
/*! The radius of an un draggable point */
#define POINT_RADIUS_UNDRAGGABLE 4

/*! The length of a line's normal       */
#define NORMAL_LENGTH 24


/* Colours have format 0xAARRGGBB*/

/*! Colour for the wall line       */
#define WALL_COLOUR       GetColour(255,255,0,0)
/*! Colour for the velocity line   */
#define VELOCITY_COLOUR   GetColour(255,0,255,0)
/*! Colour for the blocked line    */
#define BLOCKED_COLOUR    GetColour(255,0,0,255)
/*! Colour for the reflection line */
#define INTERSECTION_COLOUR GetColour(255,0,0,255)

#define CIRCLE_CENTRE_COLOUR GetColour(255, 255, 128, 0)
#define CIRCLE_RADIUS_COLOUR GetColour(255, 255, 128, 0)
#define CIRCLE_COLOUR        GetColour(255, 255, 255, 0)

/********************************** STRUCTS **********************************/

typedef struct Colour
{
	float a, r, g, b;
} Colour;

/*! Simple point game object */
typedef struct Point
{
	Vector2D pos;            /*!< The centre of this point                 */
	float radius;            /*!< The radius of this point                 */
	Colour colour;     /*!< What colour this point should draw as    */
	unsigned char draggable; /*!< If the point can be dragged by the mouse */
	unsigned char visible;   /*!< If we should draw this point             */
	unsigned char active;
	unsigned char child;
} Point;

/*! Simple line game object */
typedef struct Line
{
	Point* start;       /*!< A pointer to the starting Point         */
	Point* end;         /*!< A pointer to the ending Point           */
	LineSegment2D segment;    /*!< The actual line segment                 */
	Colour colour;      /*!< The colour of this line                 */
	unsigned char visible;    /*!< If we should draw this line             */
	unsigned char showNormal; /*!< If we should display this line's normal */
	unsigned char solid;
	unsigned char active;
} Line;

typedef struct Circle
{
	Point* centre;
	Point* radius;
	Colour colour;
	unsigned char active;
} Circle;


/********************************** GLOBALS **********************************/

/*! The mesh to use for lines  */
static AEGfxVertexList* lineMesh;
/*! The mesh to use for points */
static AEGfxVertexList* circleMesh;

/*! The array of all points */
static Point points[MAX_POINTS];
/*! The array of all lines */
static Line lines[MAX_LINES];
/*! The array of all circles */
static Circle circles[MAX_CIRCLES];

/*! The currently selected point, i.e. the one being moved by the mouse */
static Point* currentPoint;

/*! The point for the start of the moving line segment */
static Point* velocityStartPoint;
static Point* velocityEndPoint;

/********************************* PROTOTYPES ********************************/

static Point* CreatePoint(float x,
						  float y,
						  float radius,
						  unsigned char draggable,
						  Colour colour,
						  unsigned char child);

static Line* CreateLine(Point* start,
						Point* end,
						Colour colour,
						unsigned char showNormal,
						unsigned char solid);

static Line* CreateWall(float x0, float y0, float x1, float y1, Colour colour);

static Circle* CreateCircle(Point* centre, Point* radius, Colour colour);

static void GetMousePosition(Vector2D* mousePos);

static void UpdateLines(void);

static Colour GetColour(unsigned char a,
						unsigned char r,
						unsigned char g,
						unsigned char b);

static void AddWall(void);
static void AddCircle(void);
static void RemoveWall(void);
static void RemoveCircle(void);


/********************************* FUNCTIONS *********************************/

/*!****************************************************************************
\brief Loads the Test game state.  Creates point and line meshes.
******************************************************************************/
void GameStateDemoLoad(void)
{
	unsigned int i; /* Loop counter */

					/* Create the circle (point) mesh */
	AEGfxMeshStart();
	for (i = 0; i < CIRCLE_SIDES; i++)
	{
		/* The current angle */
		float a = i * TWO_PI / CIRCLE_SIDES;
		/* The next angle */
		float b = (i + 1) * TWO_PI / CIRCLE_SIDES;

		AEGfxTriAdd(0, 0, 0xFFFFFFFF, 0, 0,
					(float)sin(a), (float)cos(a), 0xFFFFFFFF, 0, 0,
					(float)sin(b), (float)cos(b), 0xFFFFFFFF, 0, 0);
	}
	circleMesh = AEGfxMeshEnd();

	/* Create the line mesh */
	AEGfxMeshStart();
	AEGfxVertexAdd(0, 0, 0xFFFFFFFF, 0, 0);
	AEGfxVertexAdd(1, 0, 0xFFFFFFFF, 0, 0);
	lineMesh = AEGfxMeshEnd();
}

/*!****************************************************************************
\brief Initialises the Test game state.
Creates the required points and lines.
******************************************************************************/
void GameStateDemoInit(void)
{
	unsigned int i; /* loop counter */
	Point* start; /* The start point of a line */
	Point* end;   /* The end point of a line   */

				  /* Reset global counters */
	currentPoint = NULL;

	for (i = 0; i < MAX_POINTS; ++i)
		points[i].active = FALSE;
	for (i = 0; i < MAX_LINES; ++i)
		lines[i].active = FALSE;
	for (i = 0; i < MAX_CIRCLES; ++i)
		circles[i].active = FALSE;

	/* Create the wall */
	CreateWall(-8, 100, -96, 32, WALL_COLOUR);
	CreateWall(96, 32, 16, 0, WALL_COLOUR);

	/* Create the velocity and intersection lines */
	velocityStartPoint = CreatePoint(-256, 32,              /* Position  */
									 POINT_RADIUS_DRAGGABLE, /* Radius    */
									 TRUE,                   /* Draggable */
									 VELOCITY_COLOUR,        /* Colour    */
									 FALSE);                 /* Child     */

	velocityEndPoint = CreatePoint(256, -64,               /* Position  */
								   POINT_RADIUS_DRAGGABLE, /* Radius    */
								   TRUE,                   /* Draggable */
								   VELOCITY_COLOUR,        /* Colour    */
								   FALSE);                 /* Child     */


	start = CreatePoint(-16, -32, POINT_RADIUS_DRAGGABLE, TRUE, CIRCLE_CENTRE_COLOUR, FALSE);
	end = CreatePoint(-24, -64, POINT_RADIUS_DRAGGABLE, TRUE, CIRCLE_RADIUS_COLOUR, FALSE);

	CreateCircle(start, end, CIRCLE_COLOUR);

	/* Update line segments and dependent points */
	UpdateLines();
}

static void AddWall()
{
	Vector2D mousePos;

	GetMousePosition(&mousePos);


	if (CreateWall(mousePos.x - 32, mousePos.y, mousePos.x + 32, mousePos.y, WALL_COLOUR))
		UpdateLines();
}

static void RemoveWall()
{
	unsigned int i;

	for (i = MAX_LINES; i > 0; --i)
	{
		Line* pLine = lines + i - 1;
		if (pLine->active && pLine->solid)
		{
			pLine->active = FALSE;
			pLine->start->active = FALSE;
			pLine->end->active = FALSE;
			UpdateLines();
			return;
		}
	}
}

static void AddCircle()
{
	Vector2D mousePos;
	Vector2D radiusPos;
	Point* centrePoint;
	Point* radiusPoint;

	GetMousePosition(&mousePos);
	Vector2DSet(&radiusPos, 32, 0);
	Vector2DAdd(&radiusPos, &radiusPos, &mousePos);

	centrePoint = CreatePoint(mousePos.x, mousePos.y, POINT_RADIUS_DRAGGABLE, TRUE, CIRCLE_CENTRE_COLOUR, FALSE);
	if (centrePoint == NULL)
		return;

	radiusPoint = CreatePoint(radiusPos.x, radiusPos.y, POINT_RADIUS_DRAGGABLE, TRUE, CIRCLE_RADIUS_COLOUR, FALSE);
	if (radiusPoint == NULL)
	{
		centrePoint->active = FALSE;
		return;
	}

	if (CreateCircle(centrePoint, radiusPoint, CIRCLE_COLOUR))
	{
		UpdateLines();
	}
	else
	{
		centrePoint->active = FALSE;
		radiusPoint->active = FALSE;
	}
}

static void RemoveCircle()
{
	unsigned int i;

	for (i = MAX_CIRCLES; i > 0; --i)
	{
		Circle* pCircle = circles + i - 1;
		if (pCircle->active)
		{
			pCircle->active = FALSE;
			pCircle->centre->active = FALSE;
			pCircle->radius->active = FALSE;
			UpdateLines();
			return;
		}
	}
}

static float GetClosestCollision(Point* startPoint, Point* endPoint, Vector2D* intersectionPoint, Vector2D* reflectionVector, void* lastObject, void** hitObject)
{
	unsigned int i;
	float t = -1;

	for (i = 0; i < MAX_LINES; ++i)
	{
		Line* pLine = lines + i;
		Vector2D intersectionPos;
		Vector2D R;
		float intersectionTime;

		if (!pLine->active || !pLine->solid || pLine == lastObject)
			continue;

		Vector2DSet(&intersectionPos, 0, 0);
		Vector2DSet(&R, 0, 0);

		intersectionTime = ReflectAnimatedPointOnStaticLineSegment(&startPoint->pos, &endPoint->pos, &pLine->segment, &intersectionPos, &R);

		if (intersectionTime != -1 && (intersectionTime < t || t == -1))
		{
			t = intersectionTime;
			*intersectionPoint = intersectionPos;
			*hitObject = pLine;
			*reflectionVector = R;
		}
	}

	for (i = 0; i < MAX_CIRCLES; ++i)
	{
		Circle* pCircle = circles + i;
		Vector2D intersectionPos;
		Vector2D R;
		float intersectionTime;

		if (!pCircle->active || pCircle == lastObject)
			continue;

		Vector2DSet(&intersectionPos, 0, 0);
		Vector2DSet(&R, 0, 0);

		intersectionTime = ReflectAnimatedPointOnStaticCircle(&startPoint->pos, &endPoint->pos, &pCircle->centre->pos, Vector2DDistance(&pCircle->centre->pos, &pCircle->radius->pos), &intersectionPos, &R);

		if (intersectionTime != -1 && (intersectionTime < t || t == -1))
		{
			t = intersectionTime;
			*intersectionPoint = intersectionPos;
			*reflectionVector = R;
			*hitObject = pCircle;
		}
	}

	return t;
}

static Point* UpdateCollision(Point* startPoint, Point* endPoint, void* lastObject)
{
	Point* intersectionPoint = NULL;
	Vector2D intersectionPos;
	Vector2D reflectionVector;
	void* hitObject;
	float t;

	t = GetClosestCollision(startPoint, endPoint, &intersectionPos, &reflectionVector, lastObject, &hitObject);

	if (t == -1)
	{
		CreateLine(startPoint, endPoint, VELOCITY_COLOUR, FALSE, FALSE);
		endPoint->colour = VELOCITY_COLOUR;
	}
	else
	{
		Vector2D reflectedPos;
		Point* reflectedPoint;

		Vector2DAdd(&reflectedPos, &intersectionPos, &reflectionVector);
		intersectionPoint = CreatePoint(intersectionPos.x, intersectionPos.y, POINT_RADIUS_UNDRAGGABLE, FALSE, INTERSECTION_COLOUR, TRUE);

		if (intersectionPoint == NULL)
			return NULL;

		reflectedPoint = CreatePoint(reflectedPos.x, reflectedPos.y, POINT_RADIUS_UNDRAGGABLE, FALSE, VELOCITY_COLOUR, TRUE);

		if (reflectedPoint == NULL)
		{
			intersectionPoint->active = FALSE;
			return NULL;
		}

		CreateLine(startPoint, intersectionPoint, VELOCITY_COLOUR, FALSE, FALSE);
		endPoint->visible = FALSE;

		UpdateCollision(intersectionPoint, reflectedPoint, hitObject);
	}

	return intersectionPoint;
}

static void UpdateLines(void)
{
	unsigned int i;
	Point* intersectionPoint;

	for (i = 0; i < MAX_POINTS; ++i)
	{
		Point* pPoint = points + i;

		if (pPoint->active && pPoint->child)
			pPoint->active = FALSE;
	}
	for (i = 0; i < MAX_LINES; ++i)
	{
		Line* pLine = lines + i;
		if (!pLine->active)
			continue;

		if (pLine->solid)
			BuildLineSegment2D(&pLine->segment, &pLine->start->pos, &pLine->end->pos);
		else
			pLine->active = FALSE;
	}

	intersectionPoint = UpdateCollision(velocityStartPoint, velocityEndPoint, NULL);

	if (intersectionPoint != NULL)
	{
		velocityEndPoint->visible = TRUE;
		CreateLine(intersectionPoint, velocityEndPoint, BLOCKED_COLOUR, FALSE, FALSE);
	}

}

/*!****************************************************************************
\brief Gets the current mouse position in world-space coordinates.
\param mousePos
A vector to receive the mouse position.
******************************************************************************/
static void GetMousePosition(Vector2D* mousePos)
{
	int mouseX, mouseY;
	float x, y;

	AEInputGetCursorPosition(&mouseX, &mouseY);
	AEGfxConvertScreenCoordinatesToWorld((float)mouseX, (float)mouseY, &x, &y);

	Vector2DSet(mousePos, x, y);
}

/*!****************************************************************************
\brief Updates the Test game state.
Checks user input, moves a selected point, and updates lines
accordingly.
******************************************************************************/
void GameStateDemoUpdate(void)
{
	Vector2D mousePos; /* The position of the mouse */

					   /* Get the mouse position */
	GetMousePosition(&mousePos);

	/* Check if we should restart or quit */
	if (AEInputCheckCurr('R'))
		gGameStateNext = GS_RESTART;
	else if (AEInputCheckCurr('Q'))
		gGameStateNext = GS_QUIT;

	if (AEInputCheckTriggered(VK_OEM_PLUS))
		AddWall();
	else if (AEInputCheckTriggered(VK_OEM_MINUS))
		RemoveWall();

	if (AEInputCheckTriggered(VK_OEM_6))
		AddCircle();
	else if (AEInputCheckTriggered(VK_OEM_4))
		RemoveCircle();


	/* If we're selecting (clicking on) a new point */
	if (currentPoint == NULL && AEInputCheckTriggered(VK_LBUTTON))
	{
		unsigned int i; /* Loop counter */
						/* For each point */
		for (i = 0; i < MAX_POINTS; i++)
		{
			Point* point = points + i; /* The current point */

			if (!point->active)
				continue;
			/* Check if we're clicking on the valid point   */
			if (point->draggable &&
				Vector2DDistance(&point->pos, &mousePos) <= point->radius)
			{
				currentPoint = point;
				break;
			}
		}
	}

	/* If there's a point to move */
	if (currentPoint != NULL)
	{
		Vector2D delta;

		Vector2DSub(&delta, &mousePos, &currentPoint->pos);

		unsigned int i; /* Loop counter */

		for (i = 0; i < MAX_CIRCLES; ++i)
		{
			Circle* pCircle = circles + i;
			if (pCircle->active && currentPoint == pCircle->centre)
				Vector2DAdd(&pCircle->radius->pos, &pCircle->radius->pos, &delta);
		}

		/* Update the point's position */
		currentPoint->pos = mousePos;

		/* Update lines and reflection for the moved point */
		UpdateLines();

		if (!AEInputCheckCurr(VK_LBUTTON) && !AEInputCheckPrev(VK_LBUTTON))
			currentPoint = NULL;
	}
}

/*!****************************************************************************
\brief Draws the Test game state.  Draws all points, lines, and normals.
******************************************************************************/
void GameStateDemoDraw(void)
{
	unsigned int i; /* Loop counter */

					/* Reset render */
	AEGfxSetBackgroundColor(0.25, 0.25, 0.25);
	AEGfxSetRenderMode(AE_GFX_RM_COLOR);


	/* Draw each circle */
	for (i = 0; i < MAX_CIRCLES; i++)
	{
		Circle* circle = circles + i;
		Point* centre;
		Point* radiusPoint;
		Matrix2D trans, scale, transform;
		Colour colour;
		float radius;

		if (!circle->active)
			continue;

		centre = circle->centre;
		radiusPoint = circle->radius;
		colour = circle->colour;
		radius = Vector2DDistance(&centre->pos, &radiusPoint->pos);;

		Matrix2DTranslate(&trans, centre->pos.x, centre->pos.y);
		Matrix2DScale(&scale, radius, radius);
		Matrix2DConcat(&transform, &trans, &scale);


		/* Set colour */
		AEGfxSetTintColor(colour.r, colour.g, colour.b, colour.a);

		/* Draw the point */
		AEGfxSetTransform(transform.m);
		AEGfxMeshDraw(circleMesh, AE_GFX_MDM_TRIANGLES);
	}


	/* Draw each line */
	for (i = 0; i < MAX_LINES; i++)
	{
		/* The line we're drawing */
		Line* line = lines + i;
		/* Matrices for creating the line's transform */
		Matrix2D trans, rot, scale, transform;
		/* The line segment of the line */
		LineSegment2D* segment;
		/* The start position of the line */
		Vector2D* start;
		/* The end position of the line */
		Vector2D* end;
		/* The colour of the line */
		Colour colour;

		/* Skip invisible objects */
		if (!line->active || !line->visible)
			continue;

		segment = &line->segment;
		start = &segment->mP0;
		end = &segment->mP1;
		colour = line->colour;

		/* Create matrix */
		Matrix2DTranslate(&trans, start->x, start->y);
		Matrix2DRotRad(&rot, (float)atan2(end->y - start->y, end->x - start->x));
		Matrix2DScale(&scale, Vector2DDistance(start, end), 1);
		Matrix2DConcat(&transform, &rot, &scale);
		Matrix2DConcat(&transform, &trans, &transform);

		/* Set colour */
		AEGfxSetTintColor(colour.r, colour.g, colour.b, colour.a);

		/* Draw the line */
		AEGfxSetTransform(transform.m);
		AEGfxMeshDraw(lineMesh, AE_GFX_MDM_LINES);

		/* If we should draw this line's normal */
		if (line->showNormal)
		{
			/* Create normal matrix */
			Matrix2DTranslate(&trans,
							  (start->x + end->x) / 2,
							  (start->y + end->y) / 2);
			Matrix2DRotRad(&rot, (float)atan2(segment->mN.y, segment->mN.x));
			Matrix2DScale(&scale, NORMAL_LENGTH * Vector2DLength(&segment->mN), 1);
			Matrix2DConcat(&transform, &rot, &scale);
			Matrix2DConcat(&transform, &trans, &transform);

			/* Draw the normal */
			AEGfxSetTransform(transform.m);
			AEGfxMeshDraw(lineMesh, AE_GFX_MDM_LINES);
		}
	}

	/* Draw each point */
	for (i = 0; i < MAX_POINTS; i++)
	{
		/* The point we're drawing */
		Point* point = points + i;
		/* Matrices for creating the point's transform */
		Matrix2D trans, scale, transform;
		/* The colour of the point */
		Colour colour;

		/* Skip invisible objects */
		if (!point->active || !point->visible)
			continue;

		colour = point->colour;

		/* Create matrix */
		Matrix2DTranslate(&trans, point->pos.x, point->pos.y);
		Matrix2DScale(&scale, point->radius, point->radius);
		Matrix2DConcat(&transform, &trans, &scale);

		/* Set colour */
		AEGfxSetTintColor(colour.r, colour.g, colour.b, colour.a);

		/* Draw the point */
		AEGfxSetTransform(transform.m);
		AEGfxMeshDraw(circleMesh, AE_GFX_MDM_TRIANGLES);
	}

}

/*!****************************************************************************
\brief Frees the Test game state's resources.  Resets all created lines
and points.
******************************************************************************/
void GameStateDemoFree(void)
{}

/*!****************************************************************************
\brief Unloads the Test game state.  Frees the generated meshes.
******************************************************************************/
void GameStateDemoUnload(void)
{
	/* Delete meshes */
	AEGfxMeshFree(circleMesh);
	AEGfxMeshFree(lineMesh);
	/* Prevent dangling pointers */
	circleMesh = NULL;
	lineMesh = NULL;
}

/*!****************************************************************************
\brief Creates a new point to display.

\param x
The x-position of the point
\param y
The y-position of the point
\param radius
The radius of the point
\param draggable
If the point can be moved by the mouse
\param colour
The colour to display the point with

\return
Returns a pointer to the new Point.
This should not be deleted by the user.
******************************************************************************/
static Point* CreatePoint(float         x,
						  float         y,
						  float         radius,
						  unsigned char draggable,
						  Colour        colour,
						  unsigned char child)
{
	unsigned int i;

	for (i = 0; i < MAX_POINTS; ++i)
	{
		/* The new point */
		Point* pPoint = points + i;
		if (pPoint->active)
			continue;

		/* Set its position */
		Vector2DSet(&pPoint->pos, x, y);
		/* Set other properties */
		pPoint->radius = radius;
		pPoint->colour = colour;
		pPoint->draggable = draggable;
		pPoint->visible = TRUE;
		pPoint->active = TRUE;
		pPoint->child = child;

		return pPoint;
	}

	return NULL;
}

/*!****************************************************************************
\brief Creates a new line to display.

\param start
The first endpoint of the line
\param end
The second endpoint of the line
\param colour
The colour of the line.  Format is 0xAARRGGBB
\param showNormal
If this line should display its normal.

\return
Returns a pointer to the new Line.
This should not be deleted by the user.
******************************************************************************/
static Line* CreateLine(Point*        start,
						Point*        end,
						Colour        colour,
						unsigned char showNormal,
						unsigned char solid)
{
	unsigned int i;

	if (start == NULL || end == NULL)
		return NULL;

	for (i = 0; i < MAX_LINES; ++i)
	{
		/* The new line */
		Line* pLine = lines + i;
		if (pLine->active)
			continue;

		/* Create the line segment */
		/* Set other properties */
		pLine->start = start;
		pLine->end = end;
		BuildLineSegment2D(&pLine->segment, &start->pos, &end->pos);
		pLine->colour = colour;
		pLine->visible = TRUE;
		pLine->showNormal = showNormal;
		pLine->solid = solid;
		pLine->active = TRUE;

		return pLine;
	}

	return NULL;
}

static Line* CreateWall(float x0, float y0, float x1, float y1, Colour colour)
{
	Point* p1 = CreatePoint(x0, y0, POINT_RADIUS_DRAGGABLE, TRUE, colour, FALSE);
	Point* p2 = CreatePoint(x1, y1, POINT_RADIUS_DRAGGABLE, TRUE, colour, FALSE);

	if (p1 == NULL)
		return NULL;

	if (p2 == NULL)
	{
		p1->active = FALSE;
		return NULL;
	}

	return CreateLine(p1, p2, colour, TRUE, TRUE);
}

static Circle* CreateCircle(Point* centre, Point* radius, Colour colour)
{
	unsigned int i;

	for (i = 0; i < MAX_CIRCLES; ++i)
	{
		Circle* pCircle = circles + i;
		if (pCircle->active)
			continue;

		pCircle->centre = centre;
		pCircle->radius = radius;
		pCircle->colour = colour;
		pCircle->active = TRUE;

		return pCircle;
	}

	return NULL;
}

static Colour GetColour(unsigned char a,
						unsigned char r,
						unsigned char g,
						unsigned char b)
{
	Colour result;

	result.a = a / 255.0f;
	result.r = r / 255.0f;
	result.g = g / 255.0f;
	result.b = b / 255.0f;

	return result;
}
