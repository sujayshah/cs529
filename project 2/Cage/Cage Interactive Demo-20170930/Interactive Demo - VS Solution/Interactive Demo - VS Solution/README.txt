Project Title: Interactive Reflection Demo
Filename:      README.txt
Course:        CS230
Author:        Brandon Hare
Digipen Email: brandon.hare@digipen.edu
Creation Date: 7 March 2016
Purpose:       Provides information about the demo.

Copyright 2016, DigiPen Institute of Technology (USA). All rights reserved.


Description:
  This demo allows you to test your reflection functions (both line and circle)
  in an interactive setting.  This only tests point/line and point/circle, not
  circle/line nor circle/circle.


How to install:
  1) Create a copy of your project - try to keep anything to do with this
       demo separate from your real work.
  2) Copy your implementation of Math2D.c, Matrix2D.c, Vector2D.c, and
       LineSegment2D.c into the src/ folder.
  3) Compile and run the project in visual studio.



A sample executable InteractiveReflectionDemo-sample.exe is provided so you
  can compare your results to mine.  This program requires the normal DLLs.
    Don't mix up this executable with your own!


How to use the demo:
  + Click and drag any of the green, red or orange points to move the
    objects around.
  + The orange point in the centre of a yellow circle determines its position.
  + The orange point on the edge of a yellow circle determines its radius.
  + The green line represents the path of the point.  It should bounce off
    red walls and yellow circles.
  + The blue line shows the original unblocked path of the point.
  + Press '-' or '+' to remove or add new walls to the scene.
  + Press '{' or '}' to remove or add new circles to the scene.
  + Press R to reset the scene.
  + Press Q or escape to quit.


Extra notes:
  The normals will be shown at the centre of most of the lines.
  If the normal vector is not normalized, the displayed normal will probably
  extend far off screen.

  This demo uses your Vector2D, Matrix2D and LineSegment2D functions as well.
    Make sure these work or you'll crash and/or see weird behaviour.

  Remember to use a separate project to install and use this demo, it's safer
    to keep it separate from your real work.
  This means you should double-check that any bugfixes are copied to the
    correct project.
  Also, be sure not to submit the wrong project!
